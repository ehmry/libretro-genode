# DO NOT EDIT #
let readJSON = p: with builtins; fromJSON (readFile p); in [
{ name = "2048"; value = { smPath = "cores/2048/upstream"; prefetch = readJSON ./cores/2048/upstream.json; url = "https://github.com/ehmry/libretro-2048.git"; }; }
{ name = "bluemsx"; value = { smPath = "cores/bluemsx/upstream"; prefetch = readJSON ./cores/bluemsx/upstream.json; url = "https://github.com/ehmry/blueMSX-libretro.git"; }; }
{ name = "fceumm"; value = { smPath = "cores/fceumm/upstream"; prefetch = readJSON ./cores/fceumm/upstream.json; url = "https://github.com/libretro/libretro-fceumm.git"; }; }
{ name = "gambatte"; value = { smPath = "cores/gambatte/upstream"; prefetch = readJSON ./cores/gambatte/upstream.json; url = "https://github.com/ehmry/gambatte-libretro.git"; }; }
{ name = "mu"; value = { smPath = "cores/mu/upstream"; prefetch = readJSON ./cores/mu/upstream.json; url = "https://github.com/meepingsnesroms/Mu.git"; }; }
{ name = "nxengine"; value = { smPath = "cores/nxengine/upstream"; prefetch = readJSON ./cores/nxengine/upstream.json; url = "https://github.com/libretro/nxengine-libretro.git"; }; }
{ name = "oberon"; value = { smPath = "cores/oberon/upstream"; prefetch = readJSON ./cores/oberon/upstream.json; url = "https://github.com/ehmry/oberon-risc-emu.git"; }; }
{ name = "samples"; value = { smPath = "cores/samples/upstream"; prefetch = readJSON ./cores/samples/upstream.json; url = "https://github.com/libretro/libretro-samples"; }; }
{ name = "tyrquake"; value = { smPath = "cores/tyrquake/upstream"; prefetch = readJSON ./cores/tyrquake/upstream.json; url = "https://github.com/ehmry/tyrquake"; }; }
{ name = "libretro-common"; value = { smPath = "libretro-common"; prefetch = readJSON ./libretro-common.json; url = "https://github.com/libretro/libretro-common.git"; }; }
{ name = "retro_frontend"; value = { smPath = "retro_frontend"; prefetch = readJSON ./retro_frontend.json; url = "https://git.sr.ht/~ehmry/retro_frontend"; }; }
]
