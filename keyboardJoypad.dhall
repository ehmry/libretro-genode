let frontend = ./retro_frontend/package.dhall

let bind = frontend.Config.Controller.bind

in  { port =
        0
    , device =
        1
    , binds =
        [ bind "KEY_LEFT" "LEFT"
        , bind "KEY_RIGHT" "RIGHT"
        , bind "KEY_UP" "UP"
        , bind "KEY_DOWN" "DOWN"
        , bind "KEY_X" "A"
        , bind "KEY_Z" "B"
        , bind "KEY_S" "X"
        , bind "KEY_A" "Y"
        , bind "KEY_Q" "L"
        , bind "KEY_W" "R"
        , bind "KEY_ENTER" "START"
        , bind "KEY_RIGHTSHIFT" "SELECT"
        ]
    }
