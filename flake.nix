{
  description = "A flake for Genode Libretro packages";

  edition = 201909;

  inputs = {
    nixpkgs.uri = "github:ehmry/nixpkgs";
    genodepkgs.url = "git+https://gitea.c3d2.de/ehmry/genodepkgs";
  };

  outputs = { self, nixpkgs, genodepkgs }:
    let
      submoduleList = import ./submodules.nix;

      expandSubmodule = system: name:

        let
          thisSystem = builtins.getAttr system;
          nixpkgs' = thisSystem nixpkgs.legacyPackages;
          genodepkgs' = thisSystem genodepkgs.packages;

          libretroCommon = with builtins;
            let sm = getAttr "libretro-common" (listToAttrs submoduleList);
            in nixpkgs'.fetchgit {
              inherit (sm) url;
              inherit (sm.prefetch) rev sha256 fetchSubmodules;
            };

        in { smPath, prefetch, url }:
        let
          value = nixpkgs'.stdenv.mkDerivation {
            pname = name;
            version = "unstable";
            src = self;
            smSrc = nixpkgs'.fetchgit {
              inherit url;
              inherit (prefetch) rev sha256 fetchSubmodules;
            };

            inherit libretroCommon smPath;

            nativeBuildInputs = with nixpkgs'.buildPackages; [ tup pkgconfig ];

            buildInputs = with genodepkgs'; [ base os libc stdcxx ];

            configurePhase = ''
              find . -name Tupfile -delete
              mkdir -p libretro-common $smPath
              cp -r $libretroCommon/* libretro-common/
              cp -r $smSrc/* $smPath
              chmod -R +rwX libretro-common/* $smPath/
              tup init
              tup -d generate tup-builder.sh
              #echo builder generated
            '';

            buildPhase = ''
              mkdir -p $out
              ln -s $out out
              pushd .
              set -v
              source tup-builder.sh
              set +v
              popd
            '';

            dontInstall = true;
          };
        in { inherit name value; };

      expandPackagesList = { system, localSystem, crossSystem }:
        map (sm: expandSubmodule system sm.name sm.value) submoduleList;

      expandPackages = system: builtins.listToAttrs (expandPackagesList system);

      systems = [ "x86_64-genode" ];

    in {
      packages = nixpkgs.lib.forAllCrossSystems expandPackages;

      defaultPackage.x86_64-linux =
        self.packages.x86_64-linux-x86_64-genode.retro_frontend;

      hydraJobs = self.packages;

      /* checks = nixpkgs.lib.forAllCrossSystems ({ system, localSystem, crossSystem }:
           let
             packages = (builtins.getAttr system genodepkgs.packages)
               // (builtins.getAttr system self.packages);
             depot = builtins.getAttr system genode-depot.packages;
             lib = nixpkgs.lib // genodepkgs.lib;
           in import ./checks { inherit system packages depot lib; });

         testNova = nixpkgs.lib.forAllCrossSystems
           ({ system, localSystem, crossSystem }:
             let
               thisSystem = builtins.getAttr system;
               packages = (thisSystem genodepkgs.packages)
                 // (thisSystem self.packages);
               depot = thisSystem genode-depot.packages;
               lib = nixpkgs.lib // genodepkgs.lib;
               systemLib = thisSystem lib;
             in rom@{ ... }:
             systemLib.buildNovaIso {
               name = "libretro-test-nova";
               rom = {
                 config = systemLib.renderDhallInit ./checks/test.dhall "{=}";
                 "libc.lib.so" = "${depot.libc}/lib/libc.lib.so";
                 "libm.lib.so" = "${depot.libc}/lib/libm.lib.so";
                 "vfs.lib.so" = "${packages.os}/lib/vfs.lib.so";
                 retro_frontend = packages.retro_frontend
                   + "/bin/retro_frontend/retro_frontend";
               } // rom;
             });
      */

    };
}
