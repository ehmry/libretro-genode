{ tar="public/$(PUBLIC_USER)/raw/"$3"/"$4".tar.xz" }

{ print "all: " tar ".sig" }

{ print tar": "FILENAME }
{ print "	@echo tar  $@" }
{ print "	@mkdir -p $(dir $@)" }
{ print "	@tar cfJ $@ --exclude=.ARCHIVES --transform=\"s|^\\.|"$4"|\" -C $(dir $<) ." }
