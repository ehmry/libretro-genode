{ if ($1 == "local") {

{ tar="public/$(PUBLIC_USER)/bin/$(PUBLIC_ARCH)/"$3"/"$4".tar.xz" }

{ print "all: " tar ".sig" }

{ print tar": "FILENAME }
{ print "	@echo tar  $@" }
{ print "	@mkdir -p $(dir $@)" }
{ print "	@tar cfJ $@ --exclude=.ARCHIVES --transform=\"s|^\\.|"$4"|\" -C $(dir $<) ." }

}}
