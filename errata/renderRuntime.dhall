let Genode = env:DHALL_GENODE

let types = env:DHALL_GENODE_TYPES

let XML = Genode.Prelude.XML

in  λ(runtime : types.Runtime) → XML.render (Genode.Runtime.toXML runtime)
