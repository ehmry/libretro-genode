let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let Frontend = ./../../retro_frontend/package.dhall

let defaults = ./../../pkgDefaults.dhall

let tyquake = ./../../cores/tyrquake/pkg.dhall

let frontend =
        tyquake.frontend
      ⫽ { vfs =
            XML.text
            ''
            <vfs>
            	<tar name="quake_shareware.tar"/>
            	<dir name="dopa"> <tar name="quake_dopa.tar"/> </dir>
            	<fs/>
            	<dir name="dev"> <log label="core"/> </dir>
            </vfs>
            ''
        , game =
            XML.text "<game path="dopa/pak0.pak"/>"
        }

in  { indexInfo =
        "Quake Episode 5: Dimension of the Past (id1/pak1.pak required)"
    , runtime =
          tyquake.runtime
        ⫽ { config =
              Frontend.Config.toXML frontend
          , roms =
              tyquake.runtime.roms # [ "quake_dopa.tar" ]
          }
    , frontend =
        frontend
    }
