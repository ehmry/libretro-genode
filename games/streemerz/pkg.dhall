let Genode = env:DHALL_GENODE

let XML = Genode.Prelude.XML

let Frontend = ./../../retro_frontend/package.dhall

let fceumm = ./../../cores/fceumm/pkg.dhall

let frontend =
        fceumm.frontend
      ⫽ { vfs =
            XML.text
            ''
            <vfs>
            	<rom name="streemerz-v02.nes"/>
            	<dir name="dev"> <log label="core"/> </dir>
            </vfs>
            ''
        }

in  { indexInfo =
        "Streemerz remake"
    , runtime =
          fceumm.runtime
        ⫽ { config =
              Frontend.Config.toXML frontend
          , roms =
              fceumm.runtime.roms # [ "streemerz-v02.nes" ]
          }
    , frontend = frontend
    }
