let defaults = ./../../pkgDefaults.dhall

let XML = (env:DHALL_GENODE).Prelude.XML

let fceumm = ./../../cores/snes9x/pkg.dhall

in  { indexInfo =
        "SNES tests"
    , runtime =
          defaults.runtime
        ⫽ { roms =
              [ "240pSuite.sfc", "stdcxx.lib.so" ]
          , config =
              XML.text
              ''
              	  <config>
              		<libc stdout="/dev/log" stderr="/dev/log"/>
              		<game/>
              		<vfs>
              			<rom name="240pSuite.sfc"/>
              			<dir name="dev"> <log label="core"/> </dir>
              		</vfs>
              		<default-controller port="0" device="1">
              			<map from="KEY_LEFT" to="LEFT"/>
              			<map from="KEY_RIGHT" to="RIGHT"/>
              			<map from="KEY_UP" to="UP"/>
              			<map from="KEY_DOWN" to="DOWN"/>

              			<map from="KEY_X" to="A"/>
              			<map from="KEY_Z" to="B"/>
              			<map from="KEY_S" to="X"/>
              			<map from="KEY_A" to="Y"/>
              			<map from="KEY_Q" to="L"/>
              			<map from="KEY_W" to="R"/>

              			<map from="KEY_ENTER" to="START"/>
              			<map from="KEY_RIGHTSHIFT" to="SELECT"/>
              		</default-controller>
              	</config>
              ''
          }
    }
