let Genode = env:DHALL_GENODE

let XML = (env:DHALL_GENODE).Prelude.XML

let require = Genode.Runtime.Require.require

let defaults = ./../../pkgDefaults.dhall

in  { indexInfo =
        "The Oldskool PC Benchmark - DOSBox"
    , runtime =
          defaults.runtime
        ⫽ { resources =
              { caps = 192, ram = Genode.units.MiB 96 }
          , requires =
              defaults.runtime.requires # [ require "report", require "rtc" ]
          , roms =
              [ "TOPBV38H.tar", "stdcxx.lib.so" ]
          , config =
              XML.text
              ''
              <config ld_verbose="yes">
              <game path="/dosbox.conf"/>
              <libc stdout="/dev/log" stderr="/dev/log" rtc="/dev/rtc"/>
              <vfs>
              <inline name="dosbox.conf">
              [autoexec]
              mount c /drive_c
              c:
              TOPBENCH.EXE
              </inline>
              <dir name="drive_c">
              <tar name="TOPBV38H.tar"/>
              <fs/>
              </dir>
              <dir name="dev"> <log label="core"/> <rtc/> </dir>
              </vfs>
              <default-controller port="0" device="771"/>
              </config>
              ''
          }
    }
