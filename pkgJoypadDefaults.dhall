let Genode = env:DHALL_GENODE

let Frontend = ./retro_frontend/package.dhall

let Prelude = Genode.Prelude

let XML = Prelude.XML

let defaults = ./pkgDefaults.dhall

let frontend =
      { vfs =
          XML.text
          ''
          <vfs>
          	<fs/>
          	<dir name="dev"> <log label="core"/> </dir>
          </vfs>
          ''
      , libc =
          XML.text "<libc stdout=\"/dev/log\" stderr=\"/dev/log\"/>\n"
      , defaultController =
          Some ./keyboardJoypad.dhall
      , game =
          XML.text "<game/>"
      , variables = [] : List { mapKey : Text, mapValue : Text }
      }

in  { runtime =
        defaults.runtime ⫽ { config = Frontend.Config.toXML frontend }
    , frontend =
        frontend
    }
