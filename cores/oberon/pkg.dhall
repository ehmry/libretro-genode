let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let Frontend = ./../../retro_frontend/package.dhall

let defaults = ./../../pkgKeyboardDefaults.dhall

let frontend =
        defaults.frontend
      ⫽ { vfs =
            XML.text
            ''
            <vfs>
            	<rom name="Oberon-2019-01-21.dsk"/>
            	<dir name="dev"> <log label="core"/> </dir>
            </vfs>
            ''
        , game =
            XML.text "<game path=\"/Oberon-2019-01-21.dsk\"/>"
        }

in  { indexInfo =
        "Oberon RISC emulator (requires three-button mouse)"
    , runtime =
          defaults.runtime
        ⫽ { config =
              Frontend.Config.toXML frontend
          , resources =
              { caps = 256, ram = Genode.units.MiB 32 }
          , roms =
              defaults.runtime.roms # [ "Oberon-2019-01-21.dsk" ]
          }
    , frontend =
        frontend
    }
