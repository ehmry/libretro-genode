let Genode = env:DHALL_GENODE

let XML = Genode.Prelude.XML

let defaults = ./../../pkgJoypadDefaults.dhall

in    defaults
    ⫽ { indexInfo =
          "Port of Gregory Montoir's Flashback emulator, running as a libretro core"
      , runtime =
            defaults.runtime
          ⫽ { roms =
                defaults.runtime.roms # [ "stdcxx.lib.so" ]
            }
      }
