let XML = (env:DHALL_GENODE).Prelude.XML

let defaults = ./../../pkgDefaults.dhall

in  { indexInfo =
        "Cave Story engine"
    , runtime =
          defaults.runtime
        ⫽ { config =
              XML.text
              ''
              <config>
              <libc stdout="/dev/log" stderr="/dev/log"/>
              <vfs>
              <tar name="doukutsu.tar"/>
              <fs/>
              <dir name="dev"> <log/> </dir>
              </vfs>
              <game/>
              <default-controller port="0" device="1">
              <map from="KEY_LEFT" to="LEFT"/>
              <map from="KEY_UP" to="UP"/>
              <map from="KEY_DOWN" to="DOWN"/>
              <map from="KEY_RIGHT" to="RIGHT"/>
              <map from="KEY_Z" to="B"/>
              <map from="KEY_X" to="A"/>
              <map from="KEY_S" to="X"/>
              <map from="KEY_Q" to="L"/>
              <map from="KEY_W" to="R"/>
              <map from="KEY_RIGHTSHIFT" to="SELECT"/>
              <map from="KEY_ENTER" to="START"/>
              </default-controller>
              </config>
              ''
          }
    }
