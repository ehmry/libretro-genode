let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let Frontend = ./../../retro_frontend/package.dhall

let defaults = ./../../pkgDefaults.dhall

let keyboardDefaults = ./../../pkgKeyboardDefaults.dhall

let frontend =
        keyboardDefaults.frontend
      ⫽ { vfs =
            XML.text
            ''
            <vfs>
            	<tar name="quake_shareware.tar"/>
            	<fs/>
            	<dir name="dev"> <log label="core"/> </dir>
            </vfs>
            ''
        , game =
            XML.text "<game/>"
        , variables =
            [ { mapKey = "tyrquake_resolution", mapValue = "960x600" } ]
        }

in  { indexInfo =
        "QUAKE engine"
    , runtime =
          defaults.runtime
        ⫽ { config =
              Frontend.Config.toXML frontend
          , resources =
              { caps = 256, ram = Genode.units.MiB 64 }
          , roms =
              defaults.runtime.roms # [ "quake_shareware.tar" ]
          }
    , frontend =
        frontend
    }
