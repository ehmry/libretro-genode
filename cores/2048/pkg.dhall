let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let List/map = Prelude.List.map

let Service = { service : Text, label : Optional Text }

let service = λ(s : Text) → { service = s, label = None Text }

let defaults = ./../../pkgDefaults.dhall

in  { indexInfo =
        "Threes! clone"
    , runtime =
          defaults.runtime
        ⫽ { resources =
              { caps = 128, ram = Genode.units.MiB 4 }
          , requires =
              List/map Text Service service [ "file_system", "nitpicker" ]
          , config =
              XML.text
              ''
              	<config>
              		<libc stdout="/dev/log" stderr="/dev/log"/>
              		<vfs>
              			<dir name="dev"> <log label="core"/> </dir>
              			<fs/>
              		</vfs>
              		<default-controller port="0" device="1">
              			<map from="KEY_LEFT" to="LEFT"/>
              			<map from="KEY_RIGHT" to="RIGHT"/>
              			<map from="KEY_UP" to="UP"/>
              			<map from="KEY_DOWN" to="DOWN"/>
              			<map from="KEY_ENTER" to="START"/>
              			<map from="KEY_RIGHTSHIFT" to="SELECT"/>
              		</default-controller>
              	</config>
              ''
          }
    }
