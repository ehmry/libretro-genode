This directory contains core submodules and Genode-specific build rules. To port 
a core to Genode, add a minimal Tupfile to the upsteam repository, add a new 
directory containing the upstream Git repository here as submodule, then add a 
Tuprules.tup file in the local directory containg Genode build rules.

The upstream `Tupfile` should be as simple as possible. It must mirror the core 
*TARGET_NAME* defined in the Libretro makefile, the *CORE_DIR* if defined by 
Make as well, and the *include_rules* directive.

```tup
TARGET_NAME=2048
CORE_DIR=$(TUP_CWD)
include_rules
```

The local `Tuprules.tup` file contains the actual build rules. Typically a core 
repository contains `Makefile` and `Makefile.common` files, the later containing 
a list of sources files relative to a `CORE_DIR` variable, which can usually be 
included and parsed by Tup. Note that Tup cannot include files relative to a 
variable, `Tuprules.tup` must include `Makefile.common` relative to itself. 
Please refer to a `Tuprules.tup` in any of the core directories here as an 
example.

Unfortunately not all cores are trivally compatible with Tup, in particular 
those with makefiles that wrap a pre-Libretro Make build system.
