let Genode = env:DHALL_GENODE

let Prelude = Genode.Prelude

let XML = Prelude.XML

let defaults = ./../../pkgDefaults.dhall

in  { indexInfo =
        "Game Boy Advance Emulator"
    , runtime =
          defaults.runtime
        ⫽ { resources =
              { caps = 128, ram = Genode.units.MiB 64 }
          , roms =
              defaults.runtime.roms # [ "stdcxx.lib.so" ]
          , config =
              XML.text
              ''
              	<config>
              		<libc stdout="/dev/log" stderr="/dev/log"/>
              		<game/>
              		<vfs>
              			<fs/>
              			<dir name="dev"> <log label="core"/> </dir>
              		</vfs>
              		<default-controller port="0" device="1">
              			<map from="KEY_LEFT" to="LEFT"/>
              			<map from="KEY_RIGHT" to="RIGHT"/>
              			<map from="KEY_UP" to="UP"/>
              			<map from="KEY_DOWN" to="DOWN"/>
              
              			<map from="KEY_Z" to="B"/>
              			<map from="KEY_X" to="A"/>
              			<map from="KEY_A" to="L"/>
              			<map from="KEY_S" to="R"/>
              
              			<map from="KEY_ENTER" to="START"/>
              			<map from="KEY_RIGHTSHIFT" to="SELECT"/>
              		</default-controller>
              	</config>
                            ''
          }
    }
