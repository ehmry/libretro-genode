let Genode = env:DHALL_GENODE

let defaults = ./../../pkgJoypadDefaults.dhall

in  { indexInfo =
        "Sega Saturn emulator"
    , runtime =
          defaults.runtime
        ⫽ { resources = { caps = 128, ram = Genode.units.MiB 32 } }
    }
