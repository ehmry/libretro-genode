let Genode = env:DHALL_GENODE

let defaults = ./../../pkgJoypadDefaults.dhall

in  defaults // { indexInfo =
        "NES emulator"
    , runtime =
          defaults.runtime
        ⫽ { resources = { caps = 128, ram = Genode.units.MiB 16 } }
    }
