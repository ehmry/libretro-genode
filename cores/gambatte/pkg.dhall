let Genode = env:DHALL_GENODE

let defaults = ./../../pkgJoypadDefaults.dhall

in  { indexInfo =
        "Game Boy Emulator"
    , runtime =
          defaults.runtime
        ⫽ { resources =
              { caps = 128, ram = Genode.units.MiB 8 }
          , roms =
              defaults.runtime.roms # [ "stdcxx.lib.so" ]
          }
    }
