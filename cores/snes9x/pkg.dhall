let Genode = env:DHALL_GENODE

let XML = Genode.Prelude.XML

let defaults = ./../../pkgJoypadDefaults.dhall

in    defaults
    ⫽ { indexInfo =
          "SNES emulator"
      , runtime =
            defaults.runtime
          ⫽ { resources =
                { caps = 192, ram = Genode.units.MiB 24 }
            , roms =
                defaults.runtime.roms # [ "stdcxx.lib.so" ]
            }
      }
