let Genode = env:DHALL_GENODE

let types = env:DHALL_GENODE_TYPES

let Prelude = Genode.Prelude

let List/map = Prelude.List.map

let Runtime/Require = types.Runtime/Require

in  { runtime =
          Genode.Runtime.defaults
        ⫽ { binary =
              "retro_frontend"
          , requires =
              List/map
              Text
              Runtime/Require
              Genode.Runtime.Require.require
              [ "audio_out", "file_system", "nitpicker", "rm" ]
          , resources = { caps = 128, ram = Genode.units.MiB 32 }
          , roms =
              [ "libc.lib.so"
              , "libm.lib.so"
              , "libretro.so"
              , "vfs.lib.so"
              ]
          }
    }
